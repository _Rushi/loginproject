<?php
session_start();
include 'config.php';
echo '<script type ="text/JavaScript">';
echo 'alert("Welcome User")';
echo '</script>';
?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Display User</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>


        <!-- <script type="text/javascript">
            $(document).ready(function(){
                $('#delete').click(function(){
                    var del=$(this).attr("id");

                    $.ajax(
                        {
                            url:'C:\wamp64\www\assignment\controller\delete.php',
                            method:"post",
                            data:{id:del},
                            dataType:"html",
                            success:function(data){
                                $('#display').html(data)
                            }
                        }
                    )

                })
            })

        </script> -->

    </head>
    <body>


<div class="container">
<!-- <p id="message"></p> -->
<table class="table" style="margin-top:80px;">
<thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Profile Photo</th>
        <th scope="col">First Name</th>
        <th scope="col">Last Name</th>
        <th scope="col">Email</th>
        <th scope="col">Mobile Number</th>
        <th scope="col">Gender</th>
        <th scope="col">Hobbies</th>
        <th scope="col">Designation</th>
        <th scope="col">Password</th>
    </tr>
</thead>
<tbody>
<?php
$sql = "select * from userdata";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) 
{
    $id = $row['Id'];
    $profilephoto = $row['image'];
    $firstname = $row['f_name'];
    $lastname = $row['l_name'];
    $email = $row['email'];
    $mobilenumber = $row['contact_number'];
    $gender = $row['gender'];
    $hobbies = $row['hobbies'];
    $designation = $row['designation'];
    $pass = $row['password']; ?>
    <tr>
            <th scope="row"><?php ECHO $id; ?></th>
            <td> <?php echo "<img src='asset/uploaded_image/$profilephoto' width=60 height=60>"; ?></td>
            <td><?php echo $firstname; ?></td>
            <td><?php echo $lastname; ?> </td>
            <td><?php echo $email; ?> </td>
            <td><?php echo $mobilenumber; ?> </td>
            <td><?php echo $gender; ?> </td>
            <td><?php echo $hobbies; ?> </td>
            <td><?php echo $designation; ?> </td>
            <td><?php echo $pass; ?> </td>

            <form action="update.php" method="get">
                <input type="hidden"  name="id"  value="<?php echo $row['Id']?>"> 
            <td><button class="btn btn-primary">Update</button></td>
            </form>
            <form action="delete.php" method="post">
            <input type="hidden"  name="id"  value="<?php echo $row['Id']?>">
            <td>
                <input type="submit"  name="delete" value="delete" id="delete" class="btn btn-danger">
            </td>
            </form>
    </tr>

    <?php
}
?>


</tbody>
</table>
</div>
<button class="btn btn-primary" name="logout"value="logout"style="margin-top:50px;  ">
    <a href="logout.php" class="text-light"  >Log Out</a>
   </button>
</body>