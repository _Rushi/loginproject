<!-- Done -->
<?php
  //including file which fire query and check if username and pass available in database or not
  
  include 'loginsucces.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LogIn Page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="/login.php/index.css">
</head>
  <body>
    <div class="container">
    <div class="login">
        <h1>LogIn</h1>
        <form action="/login.php/index.php" method="POST">
        <div class="input-class">
            <label for="">Email</label><br>
            <input style="width: 100%;" type="text" placeholder="Enter your Email" name="email" id="email" required></br>
        </div>
        <div class="input-class">
            <label for="">Password</label><br>
            <input type="password" style=" width: 100%  ;"id="password" name="password" placeholder="Enter password" required></br>
        </div>
        <button type="submit" name="login" value="login">Log In</button>
        </form>
    </div>
    <p style="text-align:center;color:white;margin-top:20px">New User? <a href="/login.php/registration.php">Register here</a></p>
    </div>
  </body>
</html>
