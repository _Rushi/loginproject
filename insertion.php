<?php

//Checking for the database connection.
require_once "config.php";

//assigning mendatory error variable with blank vaalue
$profilepic_error = $f_name_error = $l_name_error = $email_error = $contact_number_error = $password_error = $confirm_password_error = "False";

$gender_error = $hobbies_error = $designation_error = "";

if (isset($_POST['signup'])) {
// print_r($_FILES);
    // exit();

    $f_name = $_POST['f_name'];
    $l_name = $_POST['l_name'];
    $email = $_POST['email'];
    $contact_number = $_POST['contact_number'];
    $gender = $_POST['Gender'];
    // $hobbies = $_POST['Hobbies'];
    // print_r($_POST['Hobbies']);
    // $hobbies = implode(", ", $_POST['Hobbies']);
    $designation = $_POST['designation'];
    $password = $_POST['password'];
    $confirm_password = $_POST['confirm_password'];
    // profilepic => it's input place name propertie , name=>name of  file uploaded by user
    // echo ($_FILES['profilepic']);
    // $profilepic = $_FILES['profilepic']['name'];

    
    //image validtion  and storing in server folder
    if (empty($_FILES['profilepic']['name'])) {

        if (($_POST['Gender']) == "Male") {
            $profilepic = "male.jfiff";
        } elseif (($_POST)['Gender'] == "Female") {
            $profilepic = "female.jfiff";
        }

    }
    else{
        $profilepic=$_FILES['profilepic']['name'];
        $tmp_name=$_FILES['profilepic']['tmp_name'];
        $location = "asset/uploaded_image/".$profilepic;
        move_uploaded_file($tmp_name,$location);
    }
    
    // if(empty($_FILES['profilephoto']['name']))
    //     {
    //             $profilephoto="../images/swiss1.jpg";
              
    //     }
        

    // First Name Validation
    if (empty($f_name)) {
        // echo "Please enter a first name.<br>";
        $f_name_error = "true";
    } elseif (!preg_match("/^[a-zA-Z ]*$/", $f_name)) {
        $f_name_error = "true";
        echo '<script type ="text/JavaScript">';
        echo 'alert("yesss")';
        echo '</script>';
        // echo "Only letters and white space allowed in the first name.<br>";
    } else {
        // echo "First name is valid.<br>";
        $f_name = $_POST['f_name'];
    }

    //last name validation
    if (empty($l_name)) {
        echo '<script type ="text/JavaScript"> alert("Please enter a first name") </script>';
        $l_name_error = "true";
    } elseif (!preg_match("/^[a-zA-Z ]*$/", $l_name)) {
        $l_name_error = "true";
        echo '<script type ="text/JavaScript"> alert("Only letters and white space allowed in the first name") </script>';
    } else {
        // echo "Last name is valid.<br>";
        $f_name = $_POST['f_name'];
    }

    //E-mail Validation
    if (empty($email)) {
        $email_error = "true";
        echo '<script type ="text/JavaScript"> alert("Please enter an email") </script>';
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $email_error = "true";
        echo '<script type ="text/JavaScript"> alert("Invalid email format") </script>';
    } else {
        $email = $_POST['email'];
        // echo "email is valid";
    }

    //Mobile validation
    if (empty($contact_number)) {
        $contact_number_error = "true";
        echo '<script type ="text/JavaScript"> alert("Please enter a mobile number") </script>';
    } elseif (!preg_match("/^[0-9]{10}$/", $contact_number)) {
        $contact_number_error = "true";
        echo '<script type ="text/JavaScript"> alert("Only letters and white space allowed in the first name") </script>';
    } else {
        // echo "Mobile number is valid.<br>";
        $contact_number = $_POST['contact_number'];
    }
    //Hobbies validation 
    if(isset($_POST['Hobbies'])){
        $hobbies= implode(", ",$_POST['Hobbies']);
    }else{
        $hobbies = "";
    }

    //Password Validation
    if (empty($password)) {
        $password_error = "true";
        echo '<script type ="text/JavaScript"> alert("Please enter a password") </script>';
    } elseif (strlen($password) < 8) {
        $password_error = "true";
        echo '<script type ="text/JavaScript"> alert("Password must be at least 8 characters long") </script>';
    } elseif (!preg_match("#[0-9]+#", $password)) {
        $password_error = "true";
        echo '<script type ="text/JavaScript"> alert("Password must include at least one number") </script>';
    } elseif (!preg_match("#[a-z]+#", $password)) {
        $password_error = "true";
        echo '<script type ="text/JavaScript"> alert("Password must include at least one lowercase letter") </script>';
    } elseif (!preg_match("#[A-Z]+#", $password)) {
        $password_error = "true";
        echo '<script type ="text/JavaScript"> alert("Password must include at least one") </script>';
    } elseif (!preg_match("#\W+#", $password)) {
        $password_error = "true";
        echo '<script type ="text/JavaScript"> alert("Password must include at least one special character") </script>';
    } else {
        // echo "password is valid";
        $password = $_POST['password'];
    }

    //confirm password validation
    if (empty($confirm_password)) {
        echo '<script type ="text/JavaScript"> alert("Please enter confirm password") </script>';
        $confirm_password_error = "true";
    } elseif ($password != $confirm_password) {
        echo '<script type ="text/JavaScript"> alert("Please confirm the password") </script>';
        $confirm_password_error = "true";
    } else {
        $confirm_password = $_POST['confirm_password'];
        $hash_password = password_hash($password, PASSWORD_DEFAULT);
        // echo "confirm password is valid";
    }

    if (($f_name_error == "False") && ($l_name_error == "False") && ($email_error == "False") && ($contact_number_error == "False") && ($password_error == "False") && ($confirm_password_error == "False")) {
        $sql = "INSERT INTO `userdata` (`image`, `f_name`, `l_name`, `email`, `contact_number`, `gender`, `hobbies`, `designation`, `password`)
        VALUES ('$profilepic','$f_name','$l_name','$email','$contact_number','$gender','$hobbies','$designation','$hash_password')";
        
        if (mysqli_query($conn, $sql)) {
            // echo "Data Iinserted";
            echo "<script>
                var username = '$email';
                var password = '$password';
                alert('Data Inserted '+ '  Username : ' + username + '  Password : ' + password);
                window.location.href = 'index.php';
                </script>";
            // header('location:index.php');
        } else {
            echo "hemloooo data insertion fail";
        }
    }

}
