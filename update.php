<?php

    include 'config.php';
    

    $id=$_GET['id'];
    $sql="SELECT * from userdata where id=$id";
    $result = $conn-> query($sql);
    $row = $result->fetch_assoc();
    
    $id = $row['Id'];
    $profilephoto = $row['image'];
    $firstname = $row['f_name'];
    $lastname = $row['l_name'];
    $email = $row['email'];
    $contact_num = $row['contact_number'];
    $gender = $row['gender'];
    $hobbies = $row['hobbies'];
    $designation = $row['designation'];
    // $profilephoto=$row['profile photo'];
    // $firstname=$row['first name'];
    // $lastname=$row['last name'];
    // $email=$row['email'];
    // $mobilenumber=$row['mobile number'];
    // $gender=$row['gender'];
    // $hobbies=$row['hobbies'];
    // $designation=$row['designation'];
    // $password=$row['password'];


    if(isset($_POST['signup'])){
        $profilephoto=$_POST['image'];
        $firstname=$_POST['firstname'];
        $lastname=$_POST['lastname'];
        $email=$_POST['email'];
        $contact_num=$_POST['contact_number'];
        $gender=$_POST['gender'];
        $hobbies=$_POST['hobbies'];
        $designation=$_POST['designation'];
        // $password=$_POST['password'];

        $sql="UPDATE userdata set `profile photo`='$profilephoto',`f_name`='$firstname',`l_name`='$lastname',`email`='$email',`contact_number`='$contact_num', `gender`='$gender',`hobbies`='$hobbies',`designation`='$designation' where id=$id ";

        

        $result=$conn->query($sql);

        
    if($result)
    {
        //echo "Data updated successfully";
        header('location:../view/display.php');
    }
    else{
        die(mysqli_error($conn));
    }
    }

    


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration Page</title>
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="update.css">
</head>
<body>
   
    <div class="signup-box">
        <h1>Update</h1>
        <form action="" method="post">
                <label for="">Upload profile photo</label>
                <input style=" width: 90%;"type="file" name="profilephoto" value="<?php echo $profilephoto   ?>"></br>
                

                
                <label for="">First Name</label>
                <input style=" width: 90%;"type="text" placeholder="Enter First Name" name="firstname" value="<?php echo $firstname   ?>" required></br>
                

                
                <label for="">Last Name</label>
                <input style=" width: 90%;" type="text" placeholder="Enter Last Name" name="lastname" value="<?php echo $lastname   ?>"required></br>
                

                
                <label for="">Email</label>
                <input style=" width: 90%;"type="text" placeholder="Enter your Email" name="email" value="<?php echo $email  ?>" required></br>
                

                
                <label for="">Mobile Number</label>
                <input style=" width: 90%;"type="text" placeholder="Enter your Mobile Number" name="contact_number" value="<?php echo $contact_num   ?>"  required></br>
                

                <label for="">Gender</label>
                <input type="radio" name="gender"  value="male" <?php echo ($gender=='Male')?'checked':'' ?>  >Male
                <input type="radio" name="gender"  value="female" <?php echo ($gender=='female')?'checked':'' ?>  >Female
                <input type="radio" name="gender"  value="other" <?php echo ($gender=='other')?'checked':'' ?> >Other
                </br>
                
                <label for="">Hobbies</label>
                <input type="checkbox" name="hobbies" value="reading" <?php echo ($hobbies=='reading')?'checked':''  ?>>Reading
                <input type="checkbox" name="hobbies" value="dancing" <?php echo ($hobbies=='dancing')?'checked':''  ?>>Dancing
                <input type="checkbox" name="hobbies" value="traveling" <?php echo ($hobbies=='traveling')?'checked':''  ?> >Traveling
                <input type="checkbox" name="hobbies" value="singing" <?php echo ($hobbies=='singing')?'checked':''  ?>>Singing
                </br>
                
                
                <label for="designation">Designation</label>
                <select style=" width:90%  ;" name="designation" id="designation">
                    <option value="Trainee"  <?php echo ($designation == 'Trainee') ? 'selected' : ''; ?>>Trainee</option>
                    <option value="Team Leader" <?php echo ($designation == 'Team Leader') ? 'selected' : ''; ?>>Team Leader</option>
                    <option value="Project Manager" <?php echo ($designation == 'Project Manager') ? 'selected' : ''; ?>>Project Manager</option>
                    <option value="Receptionist" <?php echo ($designation == 'Receptionist') ? 'selected' : ''; ?>>Receptionist</option>
                </select>
                </br>
                
                

                
                <!-- <label for="">Password</label>
                <input style=" width:90%  ;"type="password" id="password" name="password" placeholder="Enter password" name="password" required></br>
                

               
                <label for="">Confirm Password</label>
                <input style=" width:90%; margin-bottom:30px"type="password" id="conpassword" name="conpassword" required ></br> -->
                <br>
                <br>

                <button type="submit" name="signup">Update</button>

        </form>

    </div>
   

    <p style="text-align:center;margin-top:30px;color:white;">Already have an account? <a href="#">Login here</a></p>
</body>
</html>



