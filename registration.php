
<?php
//insertion file which perform validatattion and insert into the Database
include "insertion.php";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration Page</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="update.css">
</head>
<body>

    <div class="signup-box">
    <h1>Sign Up</h1>
        <form action="" method="POST" enctype="multipart/form-data">
                
                <label for="">Upload profile photo</label>
                <input style=" width: 90%;"type="file" name="profilepic" id="profilepic" accept="image/*"></br>

                <label for="">First Name</label>
                <input style=" width: 90%;"type="text" name="f_name" id="f_name" placeholder="Enter First Name" required></br>

                <label for="">Last Name</label>
                <input name="l_name" id="l_name" type="text" style=" width: 90%;" placeholder="Enter Last Name" required></br>

                <label for="">Email</label>
                <input name="email" id="email"  type="text" style=" width: 90%;" placeholder="Enter your Email" required></br>

                <label for="">Mobile Number</label>
                <input name="contact_number" id="contact_num" type="text" style=" width: 90%;" placeholder="Enter your Mobile Number" required></br>
                
                <label for="">Gender</label>
                <input type="radio" name="Gender" id="Male"    value="Male" >Male
                <input type="radio" name="Gender" id="Female"  value="Female" >Female
                <input type="radio" name="Gender" id="Other"   value="Other" >Other
                </br>

                <label for="">Hobbies</label>
                <input type="checkbox" id="Hobbie1" name="Hobbies[]" value="Reading"><span>Reading </span>
                <input type="checkbox" id="Hobbie2" name="Hobbies[]" value="Dancing"><span>Dancing </span>
                <input type="checkbox" id="Hobbie3" name="Hobbies[]" value="Traveling"><span>Traveling</span>
                <input type="checkbox" id="Hobbie4" name="Hobbies[]" value="Singing"><span>Singing</span>
                </br>

                <label for="designation">Designation</label>
                <select style=" width:90%  ;" name="designation" id="designation">
                    <option value="Trainee">Trainee</option>
                    <option value="Team Leader">Team Leader</option>
                    <option value="Project Manager">Project Manager</option>
                    <option value="Receptionist">Receptionist</option>
                </select>
                </br>

                <label for="Password">Password</label>
                <input style=" width:90%  ;"type="password" id="password" name="password" placeholder="Enter password" required></br>

                <label for="Confirm Password">Confirm Password</label>
                <input style=" width:90%; margin-bottom:30px"type="password" id="confirm_password" name="confirm_password" required ></br>

                <button type="submit" id="signup" name="signup" value="signup" onclick="redirect()">Sign Up</button>
                
        </form>
    </div>
    <p style="text-align:center;margin-top:30px;color:white;">Already have an account? <a href="/login.php/index.php">Login here</a></p>
</body>
</html>
